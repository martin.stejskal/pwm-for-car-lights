# PWM
 * Lowering clock can cause high pitch noise from "auto LED" with integrated
   DC/DC.
 * Since some "auto LED" might contain capacitor or whole DC/DC changer, it is
   quite problematic to control their output, yet not impossible. By increasing
   resistance to base of NPN resistor it makes switching "more lazy" and
   therefore it shorten PWM pulses a bit. So instead of 1/256 pulse (PWM = 1)
   is possible to achieve something like 1/512 (or less) pulse. Shorten pulse
   means less energy to the load. But "more lazy" switching cause higher heat
   on MOS-FET, which might be problem when classic bulb would be used again.
 * The high power MOS-FET have quite huge capacitance on gate. Therefore pull
   up resistor have to be relatively low in order to reduce raising edge
   (which close MOS-FET and therefore define "close time"). Since threshold
   voltage is about -4 V, resistor divider can be used, so gate capacitance is
   not discharged to 0 V but at about +6 V (-6V source -> gate). This is enough
   to enable MOS-FET, while increase slew rate when MOS-FET is closing.

