#!/bin/bash
# RUN COMMAND FROM PROJECT ROOT DIRECTORY

# Exit when any command fails
set -e

# Configure
cmake -B build -G "Ninja Multi-Config" \
  -DCMAKE_TOOLCHAIN_FILE="cmake/generic-gcc-avr.cmake" -S .

# Set fuses
cmake --build build/ -t set_fuses --verbose
