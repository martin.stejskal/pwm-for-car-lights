/**
 * @file
 * @author Martin Stejskal
 * @brief Simple driver for LEDs
 */
#ifndef __LED_H__
#define __LED_H__
// ===============================| Includes |================================
#include <stdbool.h>
// ================================| Defines |================================

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================
typedef enum {
  LED_0,
  LED_1,
  LED_2,
  LED_3,
  LED_ALL,
} te_led_select;
// ===========================| Global variables |============================

// ===============================| Functions |===============================

// =========================| High level functions |==========================
/**
 * @brief Initialize GPIO for LEDs
 */
void led_init(void);

/**
 * @brief Enable/disable required LED
 *
 * @param e_led On of the selected LED
 * @param b_value True if you want enabled LED, false otherwise
 */
void led_set(te_led_select e_led, bool b_value);
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================

#endif  // __LED_H__
