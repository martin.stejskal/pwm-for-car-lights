/**
 * @file
 * @author Martin Stejskal
 * @brief PWM driver for ATmega16/32
 */
// ===============================| Includes |================================
#include "pwm.h"

#include <avr/io.h>

#include "config.h"
#include "gpio.h"
// ================================| Defines |================================
#define _CLK_SOURCE (CLK_PRESCALLER_1)
// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================
// =======================| Structures, enumerations |========================
/**
 * @brief Clock sources for CSx value at control register
 */
typedef enum {
  CLK_NO_SOURCE = 0,    /**< No clock */
  CLK_PRESCALLER_1,     /**< Prescaller 1x */
  CLK_PRESCALLER_8,     /**< Prescaller 8x */
  CLK_PRESCALLER_64,    /**< Prescaller 64x */
  CLK_PRESCALLER_256,   /**< Prescaller 256x */
  CLK_PRESCALLER_1024,  /**< Prescaller 1024x */
  CLK_EXT_FALLING_EDGE, /**< External clock source - falling edge */
  CLK_EXT_RISING_EDGE,  /**< External clock source - rising edge */
} te_clock_source;
// ===============================| Registers |===============================
/* Expected little endian - LSb first
 *
 * Also, register names have to use suffix, in order to not collide with what is
 * used by io.h
 */

typedef union {
  struct {
    uint8_t CS0_2_0_ : 3;
    uint8_t WGM01_ : 1;
    uint8_t COM0_1_0_ : 2;
    uint8_t WGM00_ : 1;
    uint8_t FOC0_ : 1;
  } s;
  uint8_t u8_raw;
} tu_tccr0;

typedef union {
  struct {
    uint8_t CS2_2_0_ : 3;
    uint8_t WGM21_ : 1;
    uint8_t COM2_1_0_ : 2;
    uint8_t WGM20_ : 1;
    uint8_t FOC2_ : 1;
  } s;
  uint8_t u8_raw;
} tu_tccr2;

typedef union {
  struct {
    uint8_t WGM1_1_0_ : 2;
    uint8_t FOC1B_ : 1;
    uint8_t FOC1A_ : 1;
    uint8_t COM1B_1_0_ : 2;
    uint8_t COM1A_1_0_ : 2;
  } s;
  uint8_t u8_raw;
} tu_tccr1a;

typedef union {
  struct {
    uint8_t CS1_2_0_ : 3;
    uint8_t WGM1_3_2_ : 2;
    uint8_t : 1;
    uint8_t ICES1_ : 1;
    uint8_t ICNC1_ : 1;
  } s;
  uint8_t u8_raw;
} tu_tccr1b;
// ==============================| /Registers |===============================

// ===========================| Global variables |============================

// ===============================| Functions |===============================

// ===============| Internal function prototypes: high level |================
// ==============| Internal function prototypes: middle level |===============
// ================| Internal function prototypes: low level |================
/**
 * @brief Convert percentage PWM value to raw PWM value
 *
 * @param u8_percentage Percentage value in range 0~100
 * @return Raw value in range 0~255
 */
static uint8_t _percentage_to_raw(const uint8_t u8_percentage);

static uint8_t _raw_to_percentage(const uint8_t u8_raw);

// =========================| High level functions |==========================
void pwm_init(void) {
  // ============================| Timer counter 0 |==========================
  const tu_tccr0 u_tccr0_reg = {
      // Mode fast PWM
      .s.WGM00_ = 1,
      .s.WGM01_ = 1,

      // Output mode - non inverting mode
      .s.COM0_1_0_ = 0x2,

      // Prescaling
      .s.CS0_2_0_ = _CLK_SOURCE,
  };

  TCCR0 = u_tccr0_reg.u8_raw;

  // ============================| Timer counter 1 |==========================
  const tu_tccr1a u_tccr1a_reg = {
      // Mode fast PWM - 8 bit
      .s.WGM1_1_0_ = 0x1,

      // Output mode - non inverting mode
      .s.COM1A_1_0_ = 0x2,
      .s.COM1B_1_0_ = 0x2,
  };

  const tu_tccr1b u_tccr1b_reg = {
      // Mode fast PWM - 8 bit
      .s.WGM1_3_2_ = 0x1,

      // Prescaling
      .s.CS1_2_0_ = _CLK_SOURCE,
  };

  TCCR1A = u_tccr1a_reg.u8_raw;
  TCCR1B = u_tccr1b_reg.u8_raw;

  // ============================| Timer counter 2 |==========================
  const tu_tccr2 u_tccr2_reg = {
      // Mode fast PWM
      .s.WGM20_ = 1,
      .s.WGM21_ = 1,

      // Output mode - non inverting mode
      .s.COM2_1_0_ = 0x2,

      // Prescaling
      .s.CS2_2_0_ = _CLK_SOURCE,
  };

  TCCR2 = u_tccr2_reg.u8_raw;

  // Keep all GPIO disabled by default
  // GPIO as input structure
  const te_gpio_cfg s_cfg_in = {
      .e_dir = GPIO_DIR_IN,
      .in.e_pull_up_down = GPIO_NO_PULL_UP_PULL_DOWN,
  };

  gpio_configure_pin(IO_PWM_0, s_cfg_in);
  gpio_configure_pin(IO_PWM_1, s_cfg_in);
  gpio_configure_pin(IO_PWM_2, s_cfg_in);
  gpio_configure_pin(IO_PWM_3, s_cfg_in);
}
// ========================| Middle level functions |=========================
void pwm_set(const te_pwm_channel e_ch, const uint8_t u8_percentage) {
  pwm_set_raw(e_ch, _percentage_to_raw(u8_percentage));
}

uint8_t pwm_get(const te_pwm_channel e_ch) {
  return _raw_to_percentage(pwm_get_raw(e_ch));
}

// ==========================| Low level functions |==========================
void pwm_set_raw(const te_pwm_channel e_ch, const uint8_t u8_raw_value) {
  // When PWM is set to the 0, you can still see LED shining a bit. In order
  // to completely disable PWM, set GPIO to input

  // GPIO as input structure
  const te_gpio_cfg s_cfg_in = {
      .e_dir = GPIO_DIR_IN,
      .in.e_pull_up_down = GPIO_NO_PULL_UP_PULL_DOWN,
  };

  // GPIO as output structure
  const te_gpio_cfg s_cfg_out = {
      .e_dir = GPIO_DIR_OUT,
      .out.b_out_value = 0,
  };

  // Check previous PWM value. If it was 0 and now it is not, then GPIO needs
  // to be enabled. If current value is 0 and previous was non-zero, then GPIO
  // needs to be enabled. In other cases, GPIO state can remain unchanged
  switch (e_ch) {
    case PWM_CH_0:

      if ((OCR0 == 0) && (u8_raw_value > 0)) {
        gpio_configure_pin(IO_PWM_0, s_cfg_out);

      } else if ((OCR0 != 0) && (u8_raw_value == 0)) {
        gpio_configure_pin(IO_PWM_0, s_cfg_in);
      }

      OCR0 = u8_raw_value;
      break;

    case PWM_CH_1:
      if ((OCR1AL == 0) && (u8_raw_value > 0)) {
        gpio_configure_pin(IO_PWM_1, s_cfg_out);
      } else if ((OCR1AL != 0) && (u8_raw_value == 0)) {
        gpio_configure_pin(IO_PWM_1, s_cfg_in);
      }

      // Working in 8 bit mode - no need to fill "high" register
      OCR1AL = u8_raw_value;
      break;

    case PWM_CH_2:
      if ((OCR1BL == 0) && (u8_raw_value > 0)) {
        gpio_configure_pin(IO_PWM_2, s_cfg_out);
      } else if ((OCR1BL != 0) && (u8_raw_value == 0)) {
        gpio_configure_pin(IO_PWM_2, s_cfg_in);
      }

      // Working in 8 bit mode - no need to fill "high" register
      OCR1BL = u8_raw_value;
      break;

    case PWM_CH_3:
      if ((OCR2 == 0) && (u8_raw_value > 0)) {
        gpio_configure_pin(IO_PWM_3, s_cfg_out);
      } else if ((OCR2 != 0) && (u8_raw_value == 0)) {
        gpio_configure_pin(IO_PWM_3, s_cfg_in);
      }

      OCR2 = u8_raw_value;
      break;

    case PWM_CH_ALL:
      // Do not waste time to check every single GPIO. Simply set as output if
      // PWM value is non zero, otherwise set it as input
      if (u8_raw_value) {
        gpio_configure_pin(IO_PWM_0, s_cfg_out);
        gpio_configure_pin(IO_PWM_1, s_cfg_out);
        gpio_configure_pin(IO_PWM_2, s_cfg_out);
        gpio_configure_pin(IO_PWM_3, s_cfg_out);
      } else {
        gpio_configure_pin(IO_PWM_0, s_cfg_in);
        gpio_configure_pin(IO_PWM_1, s_cfg_in);
        gpio_configure_pin(IO_PWM_2, s_cfg_in);
        gpio_configure_pin(IO_PWM_3, s_cfg_in);
      }

      OCR0 = u8_raw_value;
      OCR1AL = u8_raw_value;
      OCR1BL = u8_raw_value;
      OCR2 = u8_raw_value;
      break;

    default:
      // Do noting - invalid channel
      break;
  }
}

uint8_t pwm_get_raw(const te_pwm_channel e_ch) {
  switch (e_ch) {
    case PWM_CH_0:
      return OCR0;

    case PWM_CH_1:
      // Working in 8 bit mode - just load "low" register
      return OCR1AL;

    case PWM_CH_2:
      // Working in 8 bit mode - just load "low" register
      return OCR1BL;

    case PWM_CH_3:
      return OCR2;

    default:
      // Invalid option
      return 0;
  }
}

// ====================| Internal functions: high level |=====================
// ===================| Internal functions: middle level |====================
// =====================| Internal functions: low level |=====================
inline static uint8_t _percentage_to_raw(const uint8_t u8_percentage) {
  // If overshoot limit, then instantly return maximum possible value
  if (u8_percentage > 100) {
    return 255;
  }

  // Integer can not work with fractional numbers and using float is quite
  // expensive. Instead 16 bit value will be used, values will be multiplied
  // by 100 and therefore also rounding will be possible
  // 1% is 2.55 -> x100 -> 255
  uint16_t u16_tmp = 255 * (uint16_t)u8_percentage;

  // Rounding: +0.5 -> +50
  u16_tmp += 50;

  // Divide by 100 to get into original 8 bit range
  u16_tmp /= 100;

  return (uint8_t)u16_tmp;
}

inline static uint8_t _raw_to_percentage(const uint8_t u8_raw) {
  // Integer can not work with fractional numbers and float is quite resource
  // consuming. So simply work with 100x multiplied values. Note that
  // percentage is also fractional value, so another 100x multiplier have to be
  // applied
  uint32_t u32_tmp = (100UL * 100UL * (uint32_t)u8_raw) / 255UL;

  // Rounding
  u32_tmp += 50;

  // Divide by 100 to get into original 8 bit range
  u32_tmp /= 100;

  return (uint8_t)u32_tmp;
}
