/**
 * @file
 * @author Martin Stejskal
 * @brief Driver for non volatile memory
 */
#ifndef __NVS_H__
#define __NVS_H__
// ===============================| Includes |================================
#include "pwm.h"
// ================================| Defines |================================

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================

// ===========================| Global variables |============================

// ===============================| Functions |===============================

// =========================| High level functions |==========================
/**
 * @brief Load PWM value or specific channel from NVS
 *
 * @param e_ch Selected PWM channel
 * @return Loaded PWM value
 */
uint8_t nvs_load_pwm(const te_pwm_channel e_ch);

/**
 * @brief Save PWM value for specific channel into NVS
 *
 * @param e_ch Selected PWM channel
 * @param u8_pwm  Value to be stored
 */
void nvs_save_pwm(const te_pwm_channel e_ch, const uint8_t u8_pwm);
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================

#endif  // __NVS_H__
