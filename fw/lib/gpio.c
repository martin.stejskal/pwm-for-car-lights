/**
 * @file
 * @author Martin Stejskal
 * @brief GPIO driver for AVR MCU
 */
// ===============================| Includes |================================
#include "gpio.h"

#include <avr/io.h>
// ================================| Defines |================================
// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================

// ===========================| Global variables |============================

// ===============================| Functions |===============================

// ===============| Internal function prototypes: high level |================
// ==============| Internal function prototypes: middle level |===============
/**
 * @brief Get bit in given Byte
 *
 * @param u8_byte Input Byte
 * @param u8_bit_idx Bit index inside input Byte
 * @return True if bit is set, false otherwise
 */
static bool _get_bit(const uint8_t u8_byte, const uint8_t u8_bit_idx);

/**
 * @brief Set bit in given Byte
 *
 * @param u8_byte Input Byte
 * @param u8_bit_idx Bit index inside input Byte
 * @param b_bit_value Value which should be set
 * @return Output Byte with required modification
 */
static uint8_t _set_bit(uint8_t u8_byte, const uint8_t u8_bit_idx,
                        const bool b_bit_value);
// ================| Internal function prototypes: low level |================
/**
 * @brief Get all GPIO registers based on selected pin
 *
 * @param e_pin One of the pin from enumeration
 * @param[out] pu16_port_addr PORTx address as integer
 * @param[out] pu16_dd_addr DDx address as integer
 * @param[out] pu16_pin_addr PINx address as integer
 * @param[out] pu8_pin GPIO pin number as integer
 * @return GPIO_PIN_NOT_ON_MCU if required pin is not at current MCU. GPIO_OK
 *         if no error.
 */
static te_gpio_error _get_port_dd_pin(const te_gpio_pin e_pin,
                                      uint16_t *pu16_port_addr,
                                      uint16_t *pu16_dd_addr,
                                      uint16_t *pu16_pin_addr,
                                      uint8_t *pu8_pin);

// =========================| High level functions |==========================
te_gpio_error gpio_configure_pin(const te_gpio_pin e_pin,
                                 const te_gpio_cfg s_cfg) {
  uint8_t u8_pin;
  // Keep address to registers as integer. I was unable to solve it through
  // pointers to pointers - not sure if bug in compiler or something else
  uint16_t u16_port_addr, u16_dd_addr, u16_pin_addr;

  te_gpio_error e_err_code = _get_port_dd_pin(
      e_pin, &u16_port_addr, &u16_dd_addr, &u16_pin_addr, &u8_pin);
  if (e_err_code) {
    return e_err_code;
  }

  uint8_t *pu8_port = (uint8_t *)u16_port_addr;
  uint8_t *pu8_dd = (uint8_t *)u16_dd_addr;
  uint8_t *pu8_pin = (uint8_t *)u16_pin_addr;

  switch (s_cfg.e_dir) {
    case GPIO_DIR_HIZ:
      // Basically same as for input, but do not care about pull up/down
      *pu8_dd = _set_bit(*pu8_dd, u8_pin, 0);
      *pu8_port = _set_bit(*pu8_port, u8_pin, 0);
      break;

    case GPIO_DIR_IN:

      *pu8_dd = _set_bit(*pu8_dd, u8_pin, 0);
      switch (s_cfg.in.e_pull_up_down) {
        case GPIO_NO_PULL_UP_PULL_DOWN:
          *pu8_port = _set_bit(*pu8_port, u8_pin, 0);
          break;
        case GPIO_PULL_UP:
          *pu8_port = _set_bit(*pu8_port, u8_pin, 1);
          break;
        case GPIO_PULL_DOWN:
          // At least set as input without pull up
          *pu8_port = _set_bit(*pu8_port, u8_pin, 0);
          e_err_code = GPIO_FEATURE_NOT_SUPPORTED;
          break;
        default:
          // Unknown pull up/down option -> at least disable any pull up/down
          *pu8_port = _set_bit(*pu8_port, u8_pin, 0);
          e_err_code = GPIO_INVALID_ARG;
      }
      break;

    case GPIO_DIR_OUT:
      *pu8_dd = _set_bit(*pu8_dd, u8_pin, 1);
      *pu8_port = _set_bit(*pu8_port, u8_pin, s_cfg.out.b_out_value);
      break;

    default:
      // Unexpected direction. Do nothing
      e_err_code = GPIO_INVALID_ARG;
  }

  return e_err_code;
}
// ========================| Middle level functions |=========================
te_gpio_error gpio_set_pin(const te_gpio_pin e_pin, const bool b_out_value) {
  uint8_t u8_pin;
  uint16_t u16_port_addr, u16_dd_addr, u16_pin_addr;

  te_gpio_error e_err_code = _get_port_dd_pin(
      e_pin, &u16_port_addr, &u16_dd_addr, &u16_pin_addr, &u8_pin);
  if (e_err_code) {
    return e_err_code;
  }

  uint8_t *pu8_port = (uint8_t *)u16_port_addr;

  // Set required value
  *pu8_port = _set_bit(*pu8_port, u8_pin, b_out_value);

  return e_err_code;
}

te_gpio_error gpio_get_pin(const te_gpio_pin e_pin, bool *pb_in_value) {
  uint8_t u8_pin;
  uint16_t u16_port_addr, u16_dd_addr, u16_pin_addr;

  te_gpio_error e_err_code = _get_port_dd_pin(
      e_pin, &u16_port_addr, &u16_dd_addr, &u16_pin_addr, &u8_pin);
  if (e_err_code) {
    return e_err_code;
  }

  // Get pointer to PINx register
  uint8_t *pu8_pin = (uint8_t *)u16_pin_addr;

  *pb_in_value = _get_bit(*pu8_pin, u8_pin);

  return e_err_code;
}

// ==========================| Low level functions |==========================
te_gpio_error gpio_set_pull_up_down(const te_gpio_pin e_pin,
                                    const te_gpio_pull_up_down e_pull_up_down) {
  uint8_t u8_pin;
  uint16_t u16_port_addr, u16_dd_addr, u16_pin_addr;

  te_gpio_error e_err_code = _get_port_dd_pin(
      e_pin, &u16_port_addr, &u16_dd_addr, &u16_pin_addr, &u8_pin);
  if (e_err_code) {
    return e_err_code;
  }

  uint8_t *pu8_port = (uint8_t *)u16_port_addr;

  switch (e_pull_up_down) {
    case GPIO_NO_PULL_UP_PULL_DOWN:
      *pu8_port = _set_bit(*pu8_port, u8_pin, 0);
      break;

    case GPIO_PULL_UP:
      *pu8_port = _set_bit(*pu8_port, u8_pin, 1);
      break;

    case GPIO_PULL_DOWN:
      e_err_code = GPIO_FEATURE_NOT_SUPPORTED;
      break;

    default:
      e_err_code = GPIO_INVALID_ARG;
  }

  return e_err_code;
}

// ====================| Internal functions: high level |=====================
// ===================| Internal functions: middle level |====================
static bool _get_bit(const uint8_t u8_byte, const uint8_t u8_bit_idx) {
  // Create mask, check if zero or not
  const uint8_t u8_mask = 1 << u8_bit_idx;

  if (u8_mask & u8_byte) {
    // Bit is set
    return true;
  } else {
    return false;
  }
}

static uint8_t _set_bit(uint8_t u8_byte, const uint8_t u8_bit_idx,
                        const bool b_bit_value) {
  if (b_bit_value) {
    // Set bit
    u8_byte |= 1 << u8_bit_idx;
  } else {
    // Clear bit
    u8_byte &= ~(1 << u8_bit_idx);
  }

  return u8_byte;
}

// =====================| Internal functions: low level |=====================
static te_gpio_error _get_port_dd_pin(const te_gpio_pin e_pin,
                                      uint16_t *pu16_port_addr,
                                      uint16_t *pu16_dd_addr,
                                      uint16_t *pu16_pin_addr,
                                      uint8_t *pu8_pin) {
  te_gpio_error e_err_code = GPIO_OK;

  switch (e_pin) {
#ifdef PORTA
#ifdef PA0
    case PA_0:
      *pu16_port_addr = (uint16_t)&PORTA;
      *pu16_dd_addr = (uint16_t)&DDRA;
      *pu16_pin_addr = (uint16_t)&PINA;
      *pu8_pin = PA0;
      break;
#endif  // PA0
#ifdef PA1
    case PA_1:
      *pu16_port_addr = (uint16_t)&PORTA;
      *pu16_dd_addr = (uint16_t)&DDRA;
      *pu16_pin_addr = (uint16_t)&PINA;
      *pu8_pin = PA1;
      break;
#endif  // PA1
#ifdef PA2
    case PA_2:
      *pu16_port_addr = (uint16_t)&PORTA;
      *pu16_dd_addr = (uint16_t)&DDRA;
      *pu16_pin_addr = (uint16_t)&PINA;
      *pu8_pin = PA2;
      break;
#endif  // PA2
#ifdef PA3
    case PA_3:
      *pu16_port_addr = (uint16_t)&PORTA;
      *pu16_dd_addr = (uint16_t)&DDRA;
      *pu16_pin_addr = (uint16_t)&PINA;
      *pu8_pin = PA3;
      break;
#endif  // PA3
#ifdef PA4
    case PA_4:
      *pu16_port_addr = (uint16_t)&PORTA;
      *pu16_dd_addr = (uint16_t)&DDRA;
      *pu16_pin_addr = (uint16_t)&PINA;
      *pu8_pin = PA4;
      break;
#endif  // PA4
#ifdef PA5
    case PA_5:
      *pu16_port_addr = (uint16_t)&PORTA;
      *pu16_dd_addr = (uint16_t)&DDRA;
      *pu16_pin_addr = (uint16_t)&PINA;
      *pu8_pin = PA5;
      break;
#endif  // PA5
#ifdef PA6
    case PA_6:
      *pu16_port_addr = (uint16_t)&PORTA;
      *pu16_dd_addr = (uint16_t)&DDRA;
      *pu16_pin_addr = (uint16_t)&PINA;
      *pu8_pin = PA6;
      break;
#endif  // PA6
#ifdef PA7
    case PA_7:
      *pu16_port_addr = (uint16_t)&PORTA;
      *pu16_dd_addr = (uint16_t)&DDRA;
      *pu16_pin_addr = (uint16_t)&PINA;
      *pu8_pin = PA7;
      break;
#endif  // PA7
#endif  // PORTA
#ifdef PORTB
#ifdef PB0
    case PB_0:
      *pu16_port_addr = (uint16_t)&PORTB;
      *pu16_dd_addr = (uint16_t)&DDRB;
      *pu16_pin_addr = (uint16_t)&PINB;
      *pu8_pin = PB0;
      break;
#endif  // PB0
#ifdef PB1
    case PB_1:
      *pu16_port_addr = (uint16_t)&PORTB;
      *pu16_dd_addr = (uint16_t)&DDRB;
      *pu16_pin_addr = (uint16_t)&PINB;
      *pu8_pin = PB1;
      break;
#endif  // PB1
#ifdef PB2
    case PB_2:
      *pu16_port_addr = (uint16_t)&PORTB;
      *pu16_dd_addr = (uint16_t)&DDRB;
      *pu16_pin_addr = (uint16_t)&PINB;
      *pu8_pin = PB2;
      break;
#endif  // PB2
#ifdef PB3
    case PB_3:
      *pu16_port_addr = (uint16_t)&PORTB;
      *pu16_dd_addr = (uint16_t)&DDRB;
      *pu16_pin_addr = (uint16_t)&PINB;
      *pu8_pin = PB3;
      break;
#endif  // PB3
#ifdef PB4
    case PB_4:
      *pu16_port_addr = (uint16_t)&PORTB;
      *pu16_dd_addr = (uint16_t)&DDRB;
      *pu16_pin_addr = (uint16_t)&PINB;
      *pu8_pin = PB4;
      break;
#endif  // PB4
#ifdef PB5
    case PB_5:
      *pu16_port_addr = (uint16_t)&PORTB;
      *pu16_dd_addr = (uint16_t)&DDRB;
      *pu16_pin_addr = (uint16_t)&PINB;
      *pu8_pin = PB5;
      break;
#endif  // PB5
#ifdef PB6
    case PB_6:
      *pu16_port_addr = (uint16_t)&PORTB;
      *pu16_dd_addr = (uint16_t)&DDRB;
      *pu16_pin_addr = (uint16_t)&PINB;
      *pu8_pin = PB6;
      break;
#endif  // PB6
#ifdef PB7
    case PB_7:
      *pu16_port_addr = (uint16_t)&PORTB;
      *pu16_dd_addr = (uint16_t)&DDRB;
      *pu16_pin_addr = (uint16_t)&PINB;
      *pu8_pin = PB7;
      break;
#endif  // PB7
#endif  // PORTB
#ifdef PORTC
#ifdef PC0
    case PC_0:
      *pu16_port_addr = (uint16_t)&PORTC;
      *pu16_dd_addr = (uint16_t)&DDRC;
      *pu16_pin_addr = (uint16_t)&PINC;
      *pu8_pin = PC0;
      break;
#endif  // PC0
#ifdef PC1
    case PC_1:
      *pu16_port_addr = (uint16_t)&PORTC;
      *pu16_dd_addr = (uint16_t)&DDRC;
      *pu16_pin_addr = (uint16_t)&PINC;
      *pu8_pin = PC1;
      break;
#endif  // PC1
#ifdef PC2
    case PC_2:
      *pu16_port_addr = (uint16_t)&PORTC;
      *pu16_dd_addr = (uint16_t)&DDRC;
      *pu16_pin_addr = (uint16_t)&PINC;
      *pu8_pin = PC2;
      break;
#endif  // PC2
#ifdef PC3
    case PC_3:
      *pu16_port_addr = (uint16_t)&PORTC;
      *pu16_dd_addr = (uint16_t)&DDRC;
      *pu16_pin_addr = (uint16_t)&PINC;
      *pu8_pin = PC3;
      break;
#endif  // PC3
#ifdef PC4
    case PC_4:
      *pu16_port_addr = (uint16_t)&PORTC;
      *pu16_dd_addr = (uint16_t)&DDRC;
      *pu16_pin_addr = (uint16_t)&PINC;
      *pu8_pin = PC4;
      break;
#endif  // PC4
#ifdef PC5
    case PC_5:
      *pu16_port_addr = (uint16_t)&PORTC;
      *pu16_dd_addr = (uint16_t)&DDRC;
      *pu16_pin_addr = (uint16_t)&PINC;
      *pu8_pin = PC5;
      break;
#endif  // PC5
#ifdef PC6
    case PC_6:
      *pu16_port_addr = (uint16_t)&PORTC;
      *pu16_dd_addr = (uint16_t)&DDRC;
      *pu16_pin_addr = (uint16_t)&PINC;
      *pu8_pin = PC6;
      break;
#endif  // PC6
#ifdef PC7
    case PC_7:
      *pu16_port_addr = (uint16_t)&PORTC;
      *pu16_dd_addr = (uint16_t)&DDRC;
      *pu16_pin_addr = (uint16_t)&PINC;
      *pu8_pin = PC7;
      break;
#endif  // PC7
#endif  // PORTC
#ifdef PORTD
#ifdef PD0
    case PD_0:
      *pu16_port_addr = (uint16_t)&PORTD;
      *pu16_dd_addr = (uint16_t)&DDRD;
      *pu16_pin_addr = (uint16_t)&PIND;
      *pu8_pin = PD0;
      break;
#endif  // PD0
#ifdef PD1
    case PD_1:
      *pu16_port_addr = (uint16_t)&PORTD;
      *pu16_dd_addr = (uint16_t)&DDRD;
      *pu16_pin_addr = (uint16_t)&PIND;
      *pu8_pin = PD1;
      break;
#endif  // PD1
#ifdef PD2
    case PD_2:
      *pu16_port_addr = (uint16_t)&PORTD;
      *pu16_dd_addr = (uint16_t)&DDRD;
      *pu16_pin_addr = (uint16_t)&PIND;
      *pu8_pin = PD2;
      break;
#endif  // PD2
#ifdef PD3
    case PD_3:
      *pu16_port_addr = (uint16_t)&PORTD;
      *pu16_dd_addr = (uint16_t)&DDRD;
      *pu16_pin_addr = (uint16_t)&PIND;
      *pu8_pin = PD3;
      break;
#endif  // PD3
#ifdef PD4
    case PD_4:
      *pu16_port_addr = (uint16_t)&PORTD;
      *pu16_dd_addr = (uint16_t)&DDRD;
      *pu16_pin_addr = (uint16_t)&PIND;
      *pu8_pin = PD4;
      break;
#endif  // PD4
#ifdef PD5
    case PD_5:
      *pu16_port_addr = (uint16_t)&PORTD;
      *pu16_dd_addr = (uint16_t)&DDRD;
      *pu16_pin_addr = (uint16_t)&PIND;
      *pu8_pin = PD5;
      break;
#endif  // PD5
#ifdef PD6
    case PD_6:
      *pu16_port_addr = (uint16_t)&PORTD;
      *pu16_dd_addr = (uint16_t)&DDRD;
      *pu16_pin_addr = (uint16_t)&PIND;
      *pu8_pin = PD6;
      break;
#endif  // PD6
#ifdef PD7
    case PD_7:
      *pu16_port_addr = (uint16_t)&PORTD;
      *pu16_dd_addr = (uint16_t)&DDRD;
      *pu16_pin_addr = (uint16_t)&PIND;
      *pu8_pin = PD7;
      break;
#endif  // PD7
#endif  // PORTD
#ifdef PORTE
#ifdef PE0
    case PE_0:
      *pu16_port_addr = (uint16_t)&PORTE;
      *pu16_dd_addr = (uint16_t)&DDRE;
      *pu16_pin_addr = (uint16_t)&PINE;
      *pu8_pin = PE0;
      break;
#endif  // PE0
#ifdef PE1
    case PE_1:
      *pu16_port_addr = (uint16_t)&PORTE;
      *pu16_dd_addr = (uint16_t)&DDRE;
      *pu16_pin_addr = (uint16_t)&PINE;
      *pu8_pin = PE1;
      break;
#endif  // PE1
#ifdef PE2
    case PE_2:
      *pu16_port_addr = (uint16_t)&PORTE;
      *pu16_dd_addr = (uint16_t)&DDRE;
      *pu16_pin_addr = (uint16_t)&PINE;
      *pu8_pin = PE2;
      break;
#endif  // PE2
#ifdef PE3
    case PE_3:
      *pu16_port_addr = (uint16_t)&PORTE;
      *pu16_dd_addr = (uint16_t)&DDRE;
      *pu16_pin_addr = (uint16_t)&PINE;
      *pu8_pin = PE3;
      break;
#endif  // PE3
#ifdef PE4
    case PE_4:
      *pu16_port_addr = (uint16_t)&PORTE;
      *pu16_dd_addr = (uint16_t)&DDRE;
      *pu16_pin_addr = (uint16_t)&PINE;
      *pu8_pin = PE4;
      break;
#endif  // PE4
#ifdef PE5
    case PE_5:
      *pu16_port_addr = (uint16_t)&PORTE;
      *pu16_dd_addr = (uint16_t)&DDRE;
      *pu16_pin_addr = (uint16_t)&PINE;
      *pu8_pin = PE5;
      break;
#endif  // PE5
#ifdef PE6
    case PE_6:
      *pu16_port_addr = (uint16_t)&PORTE;
      *pu16_dd_addr = (uint16_t)&DDRE;
      *pu16_pin_addr = (uint16_t)&PINE;
      *pu8_pin = PE6;
      break;
#endif  // PE6
#ifdef PE7
    case PE_7:
      *pu16_port_addr = (uint16_t)&PORTE;
      *pu16_dd_addr = (uint16_t)&DDRE;
      *pu16_pin_addr = (uint16_t)&PINE;
      *pu8_pin = PE7;
      break;
#endif  // PE7
#endif  // PORTE
    default:
      // Clean up pointers to signalize "nothing there"
      *pu16_port_addr = *pu16_dd_addr = *pu8_pin = 0;

      e_err_code = GPIO_PIN_NOT_ON_MCU;
  }

  return e_err_code;
}
