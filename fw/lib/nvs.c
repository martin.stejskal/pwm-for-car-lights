/**
 * @file
 * @author Martin Stejskal
 * @brief Driver for non volatile memory
 */
// ===============================| Includes |================================
#include "nvs.h"

#include <avr/eeprom.h>

#include "config.h"
// ================================| Defines |================================

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================
// =======================| Structures, enumerations |========================

// ===========================| Global variables |============================
// Variables located in EEPROM
uint8_t EEMEM gu8_pwm[PWM_CH_ALL];
// ===============================| Functions |===============================

// ===============| Internal function prototypes: high level |================
// ==============| Internal function prototypes: middle level |===============
// ================| Internal function prototypes: low level |================

// =========================| High level functions |==========================
uint8_t nvs_load_pwm(const te_pwm_channel e_ch) {
  return eeprom_read_byte(&gu8_pwm[e_ch]);
}

void nvs_save_pwm(const te_pwm_channel e_ch, const uint8_t u8_pwm) {
  eeprom_update_byte(&gu8_pwm[e_ch], u8_pwm);
}
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================

// ====================| Internal functions: high level |=====================
// ===================| Internal functions: middle level |====================
// =====================| Internal functions: low level |=====================
