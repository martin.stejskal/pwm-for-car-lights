/**
 * @file
 * @author Martin Stejskal
 * @brief GPIO driver for AVR MCU
 */
#ifndef __GPIO_H__
#define __GPIO_H__
// ===============================| Includes |================================
#include <stdbool.h>
// ================================| Defines |================================

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================
typedef enum {
  PA_0,
  PA_1,
  PA_2,
  PA_3,
  PA_4,
  PA_5,
  PA_6,
  PA_7,

  PB_0,
  PB_1,
  PB_2,
  PB_3,
  PB_4,
  PB_5,
  PB_6,
  PB_7,

  PC_0,
  PC_1,
  PC_2,
  PC_3,
  PC_4,
  PC_5,
  PC_6,
  PC_7,

  PD_0,
  PD_1,
  PD_2,
  PD_3,
  PD_4,
  PD_5,
  PD_6,
  PD_7,

  PE_0,
  PE_1,
  PE_2,
  PE_3,
  PE_4,
  PE_5,
  PE_6,
  PE_7,
} te_gpio_pin;

typedef enum {
  GPIO_NO_PULL_UP_PULL_DOWN = 0,
  GPIO_PULL_UP,
  GPIO_PULL_DOWN,
} te_gpio_pull_up_down;

typedef enum {
  GPIO_DIR_HIZ = 0,
  GPIO_DIR_IN,
  GPIO_DIR_OUT,
} te_gpio_direction;

typedef enum {
  GPIO_OK = 0,
  GPIO_FAIL = 1,
  GPIO_PIN_NOT_ON_MCU,
  GPIO_INVALID_ARG,
  GPIO_FEATURE_NOT_SUPPORTED,  // Not supported by HW
} te_gpio_error;

typedef struct {
  // Mandatory option
  te_gpio_direction e_dir;

  union {
    struct {
      // If input, allow define pull up/down
      te_gpio_pull_up_down e_pull_up_down;
    } in;

    struct {
      // If output, this will be used to set output value immediately
      bool b_out_value;
    } out;
  };
} te_gpio_cfg;
// ===========================| Global variables |============================

// ===============================| Functions |===============================
// =========================| High level functions |==========================
/**
 * @brief Configure given GPIO
 *
 * @param e_pin One of the selected pin from enumeration
 * @param s_cfg Configuration structure
 *
 * @return GPIO_PIN_NOT_ON_MCU if pin is not available on current MCU,
 * GPIO_FEATURE_NOT_SUPPORTED if pull up/down request can not be processed,
 * GPIO_INVALID_ARG if value is out of enumeration valid range. GPIO_OK if no
 * error.
 */
te_gpio_error gpio_configure_pin(const te_gpio_pin e_pin,
                                 const te_gpio_cfg s_cfg);
// ========================| Middle level functions |=========================
/**
 * @brief Set output pin value
 *
 * @warning Function does not check if pin is set as output
 *
 * @param e_pin One of the selected pin from enumeration
 * @param b_out_value Output value - 0 or 1
 *
 * @return GPIO_PIN_NOT_ON_MCU if pin is not available, GPIO_OK if no error
 */
te_gpio_error gpio_set_pin(const te_gpio_pin e_pin, const bool b_out_value);

/**
 * @brief Read from pin value
 *
 * @warning Function does not check if pin is set as input
 *
 * @param e_pin One of the selected pin from enumeration
 * @param[out] pb_in_value Value from pin will be stored here
 *
 * @return GPIO_PIN_NOT_ON_MCU if pin is not available, GPIO_OK if no error
 */
te_gpio_error gpio_get_pin(const te_gpio_pin e_pin, bool *pb_in_value);
// ==========================| Low level functions |==========================

/**
 * @brief Allow dynamically enable pull up/down if pin set as input
 *
 * @warning Function does not check if pin is set as input
 *
 * @param e_pin One of the selected pin from enumeration
 * @param e_pull_up_down Enable or disable pull up/down
 * @return GPIO_PIN_NOT_ON_MCU if pin is not available on current MCU,
 * GPIO_FEATURE_NOT_SUPPORTED if pull up/down request can not be processed,
 * GPIO_INVALID_ARG if value is out of enumeration valid range. GPIO_OK if no
 * error.
 */
te_gpio_error gpio_set_pull_up_down(const te_gpio_pin e_pin,
                                    const te_gpio_pull_up_down e_pull_up_down);

#endif  // __GPIO_H__
