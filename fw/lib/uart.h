/**
 * @file
 * @author Martin Stejskal
 * @brief UART driver
 */
#ifndef __UART_H__
#define __UART_H__
// ===============================| Includes |================================
#include <stdint.h>
// ================================| Defines |================================

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================
/**
 * @brief Error codes for UART HAL layer
 *
 */
typedef enum {
  UART_OK = 0,
  UART_FAIL = 1,
  UART_TX_NOT_COMPLETE,
  UART_RX_NOT_COMPLETE,
  UART_RX_DATA_OVERRUN,
  UART_RX_PARITY_ERR,
} te_uart_error;
// ===========================| Global variables |============================

// ===============================| Functions |===============================

// =========================| High level functions |==========================
/**
 * @brief Initialize UART interface
 *
 * @return UART_OK if no error
 */
te_uart_error uart_init(void);

/**
 * @brief Blocking function that transmit data over UART
 *
 * @param pu8_buffer Pointer to data buffer
 * @param u16_size Size of data which should be sent
 *
 * @return UART_OK if no error
 */
te_uart_error uart_tx(const uint8_t *pu8_buffer, const uint16_t u16_size);
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================

#endif  // __UART_H__
