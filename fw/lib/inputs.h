/**
 * @file
 * @author Martin Stejskal
 * @brief Read value from inputs
 */
#ifndef __INPUTS_H__
#define __INPUTS_H__
// ===============================| Includes |================================
#include <stdbool.h>
// ================================| Defines |================================

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================
typedef enum {
  // Classic buttons
  INPUT_BTN_UP = 0,
  INPUT_BTN_DOWN,
  INPUT_BTN_NEXT,

  // External trigger event
  INPUT_TRIG_0,
  INPUT_TRIG_1,
  INPUT_TRIG_2,
  INPUT_TRIG_3,
} te_input_sources;
// ===========================| Global variables |============================

// ===============================| Functions |===============================

// =========================| High level functions |==========================
/**
 * @brief Initialize inputs GPIO
 */
void inputs_init(void);

/**
 * @brief Get input value from selected source
 *
 * This will already handle inversion caused by pull up resistors and so on.
 *
 * @param e_source Selected input source
 *
 * @return True if input source is in active level, false otherwise
 */
bool inputs_get(te_input_sources e_source);
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================

#endif  // __INPUTS_H__
