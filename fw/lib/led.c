/**
 * @file
 * @author Martin Stejskal
 * @brief Simple driver for LEDs
 */
// ===============================| Includes |================================
#include "led.h"

#include "config.h"
#include "gpio.h"
// ================================| Defines |================================

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================

// ===========================| Global variables |============================

// ===============================| Functions |===============================

// ===============| Internal function prototypes: high level |================
// ==============| Internal function prototypes: middle level |===============
// ================| Internal function prototypes: low level |================

// =========================| High level functions |==========================
void led_init(void) {
  // Set all GPIO to output, but keep them off
  const te_gpio_cfg s_cfg = {
      .e_dir = GPIO_DIR_OUT,
      .out.b_out_value = 0,
  };

  gpio_configure_pin(IO_LED_0, s_cfg);
  gpio_configure_pin(IO_LED_1, s_cfg);
  gpio_configure_pin(IO_LED_2, s_cfg);
  gpio_configure_pin(IO_LED_3, s_cfg);
}

void led_set(te_led_select e_led, bool b_value) {
  switch (e_led) {
    case LED_0:
      gpio_set_pin(IO_LED_0, b_value);
      break;
    case LED_1:
      gpio_set_pin(IO_LED_1, b_value);
      break;
    case LED_2:
      gpio_set_pin(IO_LED_2, b_value);
      break;
    case LED_3:
      gpio_set_pin(IO_LED_3, b_value);
      break;
    case LED_ALL:
      gpio_set_pin(IO_LED_0, b_value);
      gpio_set_pin(IO_LED_1, b_value);
      gpio_set_pin(IO_LED_2, b_value);
      gpio_set_pin(IO_LED_3, b_value);
      break;
    default:
      // Invalid configuration - nothing to do actually
      break;
  }
}
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================

// ====================| Internal functions: high level |=====================
// ===================| Internal functions: middle level |====================
// =====================| Internal functions: low level |=====================
