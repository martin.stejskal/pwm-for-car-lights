/**
 * @file
 * @author Martin Stejskal
 * @brief Driver for old school segment display
 */
#ifndef __SEGMENT_DISPLAY_H__
#define __SEGMENT_DISPLAY_H__
// ===============================| Includes |================================
#include <stdint.h>
// ================================| Defines |================================

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================

// ===========================| Global variables |============================

// ===============================| Functions |===============================

// =========================| High level functions |==========================
/**
 * @brief Initialize segment display driver
 *
 * Setup GPIO, clean up displayed digits.
 */
void seg_disp_init(void);

/**
 * @brief Refresh segment display
 *
 * It is necessary to call this function often, in order to show digits
 * correctly. Reason is that 2 segments are multiplexed, so it is necessary to
 * switch control between them.
 */
void seg_disp_refresh(void);
// ========================| Middle level functions |=========================
/**
 * @brief Set decimal value to be shown
 *
 * @param u8_value_dec Decimal value. Range 0~100. Other than that will be
 *                     displayed same way as 100 (99 with dots)
 */
void seg_disp_set_value_dec(const uint8_t u8_value_dec);

/**
 * @brief Display hexadecimal value
 *
 * @param u8_value_hex Value to be shown. Range: 00~FF
 */
void seg_disp_set_value_hex(const uint8_t u8_value_hex);

/**
 * @brief Turn off display
 *
 * Simply disable power drivers for segments.
 */
void seg_disp_turn_off(void);
// ==========================| Low level functions |==========================

#endif  // __SEGMENT_DISPLAY_H__
