/**
 * @file
 * @author Martin Stejskal
 * @brief UART driver for ATmega16/32
 */
// ===============================| Includes |================================
#include "uart.h"

#include <avr/io.h>

#include "config.h"
// ================================| Defines |================================

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================
#ifndef UART_BAUDRATE
#error "You need to define UART_BAUDRATE at config.h file"
#endif

#ifndef F_CPU
#error "You need to define F_CPU in order to know MCU core clock"
#endif
// =======================| Structures, enumerations |========================
// ===============================| Registers |===============================
/* Registers and it's continent.
 * Note that if register have only "1x 8 bit value", it is not put here, since
 * it does not make much sense. These structure should allow conveniently
 * set registers and use them directly.
 *
 * Note that "little endian" is expected -> LSb is first.
 *
 * Also, register names have to use suffix, in order to not collide with what is
 * used by io.h
 */
typedef union {
  struct {
    uint8_t MPCM_ : 1;
    uint8_t U2X_ : 1;
    uint8_t PE_ : 1;
    uint8_t DOR_ : 1;
    uint8_t FE_ : 1;
    uint8_t UDRE_ : 1;
    uint8_t TXC_ : 1;
    uint8_t RXC_ : 1;
  } s;
  uint8_t u8_raw;
} tu_ucsra;

typedef union {
  struct {
    uint8_t TXB8_ : 1;
    uint8_t RXB8_ : 1;
    uint8_t UCSZ2_ : 1;
    uint8_t TXEN_ : 1;
    uint8_t RXEN_ : 1;
    uint8_t UDRIE_ : 1;
    uint8_t TXCIE_ : 1;
    uint8_t RXCIE_ : 1;
  } s;
  uint8_t u8_raw;
} tu_ucsrb;

typedef union {
  struct {
    uint8_t UCPOL_ : 1;
    uint8_t UCSZ_0_1_ : 2;
    uint8_t USBS_ : 1;
    uint8_t UPM01_ : 2;
    uint8_t UMSEL_ : 1;
    uint8_t URSEL_ : 1;
  } s;
  uint8_t u8_raw;
} tu_ucsrc;

typedef union {
  struct {
    uint8_t UBRR_11_8 : 4;
    uint8_t : 3;
    uint8_t URSEL_ : 1;
  } s;
  uint8_t u8_raw;
} tu_ubrrh;
// ==============================| /Registers |===============================
// ===========================| Global variables |============================

// ===============================| Functions |===============================

// ===============| Internal function prototypes: high level |================
// ==============| Internal function prototypes: middle level |===============
// ================| Internal function prototypes: low level |================

// =========================| High level functions |==========================
te_uart_error uart_init(void) {
  // Reset all registers
  UDR = UCSRA = UCSRB = UCSRC = UBRRL = UBRRH = 0;

  const tu_ucsra u_ucsra_reg = {
      // Double speed - typically we want to run at 115200 and this allow to
      // reduce timing error
      .s.U2X_ = 1,
  };

  const tu_ucsrb u_ucsrb_reg = {
      // Enable TX way
      .s.TXEN_ = 1,

      // Expect 8 bit communication
      .s.UCSZ2_ = 0,
  };

  const tu_ucsrc u_ucsrc_reg = {
      // Expect 8 bit communication
      .s.UCSZ_0_1_ = 0x03,
  };

  // According to the equation in datasheet. Using double speed mode.
  // Since integer calculations are not accurate (no decimal point), small trick
  // is used here. Values are multiplied by 10 and then add rounding offset 0.5
  // -> +5. This works as primitive rounding
  const uint16_t u16_ubrr =
      (10UL * F_CPU / (8UL * UART_BAUDRATE) - 10 + 5) / 10;

  // Write values to registers
  UCSRA = u_ucsra_reg.u8_raw;
  UCSRB = u_ucsrb_reg.u8_raw;
  UCSRC = u_ucsrc_reg.u8_raw;

  // Load low 8 bits
  UBRRL = 0xFF & u16_ubrr;

  // Load upper 8 bits
  UBRRH = u16_ubrr >> 8;

  return UART_OK;
}

te_uart_error uart_tx(const uint8_t *pu8_buffer, uint16_t u16_size) {
  // Transmit Byte by Byte. Always wait for data register empty flag
  tu_ucsra u_ucsra;
  u_ucsra.u8_raw = UCSRA;

  for (uint16_t u16_send_bytes = 0; u16_send_bytes <= u16_size;
       u16_send_bytes++) {
    // Wait until data register is ready
    while (!u_ucsra.s.UDRE_) {
      u_ucsra.u8_raw = UCSRA;
    }

    UDR = pu8_buffer[u16_send_bytes];
  }

  // Wait for transmit complete
  while (!u_ucsra.s.TXC_) {
    u_ucsra.u8_raw = UCSRA;
  }

  return UART_OK;
}
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================

// ====================| Internal functions: high level |=====================
// ===================| Internal functions: middle level |====================
// =====================| Internal functions: low level |=====================
