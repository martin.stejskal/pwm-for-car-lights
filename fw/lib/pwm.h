/**
 * @file
 * @author Martin Stejskal
 * @brief PWM driver for ATmega16/32
 */
#ifndef __PWM_H__
#define __PWM_H__
// ===============================| Includes |================================
#include <stdint.h>
// ================================| Defines |================================

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================
typedef enum {
  PWM_CH_0 = 0,
  PWM_CH_1 = 1,
  PWM_CH_2 = 2,
  PWM_CH_3 = 3,
  PWM_CH_ALL = 4,
} te_pwm_channel;
// ===========================| Global variables |============================

// ===============================| Functions |===============================

// =========================| High level functions |==========================
/**
 * @brief Initialize PWM module
 */
void pwm_init(void);
// ========================| Middle level functions |=========================
/**
 * @brief Set PWM value for selected channel
 *
 * @param e_ch Selected PWM channel
 * @param u8_percentage PWM value as percentage. Range 0~100. Other values will
 *                      be treated as 100.
 */
void pwm_set(const te_pwm_channel e_ch, const uint8_t u8_percentage);

/**
 * @brief Get PWM value for selected channel
 *
 * @param e_ch PWM channel
 * @return PWM value as percentage. Returned range: 0~100
 */
uint8_t pwm_get(const te_pwm_channel e_ch);
// ==========================| Low level functions |==========================
/**
 * @brief Set PWM value for selected channel in raw format
 *
 * @param e_ch PWM channel
 * @param u8_raw_value Raw PWM value. Range: 0~255
 */
void pwm_set_raw(const te_pwm_channel e_ch, const uint8_t u8_raw_value);

/**
 * @brief Get PWM value for selected channel in raw format
 *
 * @param e_ch PWM channel
 * @return PWM as raw value. Range: 0~255
 */
uint8_t pwm_get_raw(const te_pwm_channel e_ch);

#endif  // __PWM_H__
