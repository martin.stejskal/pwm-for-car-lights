/**
 * @file
 * @author Martin Stejskal
 * @brief Print implementation for AVR
 *
 * It uses UART interface as print output
 */
// ===============================| Includes |================================
#include "print.h"

#include <stdio.h>

#include "uart.h"
// ================================| Defines |================================

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================

// ===========================| Global variables |============================

// ===============================| Functions |===============================

// ===============| Internal function prototypes: high level |================
// ==============| Internal function prototypes: middle level |===============
// ================| Internal function prototypes: low level |================
/**
 * @brief Callback function for "printf()"
 *
 * @param c_character Character to be printed
 * @param ps_stream Pointer to file like handler. This is purely software
 *                  thing, since on AVR there is no file system
 * @return Error code as integer
 */
static int _uart_put_char(char c_character, FILE *ps_stream);
// =========================| High level functions |==========================
void print_init(void) {
  static FILE mystdout =
      FDEV_SETUP_STREAM(_uart_put_char, NULL, _FDEV_SETUP_WRITE);

  stdout = &mystdout;

  uart_init();
}
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================

// ====================| Internal functions: high level |=====================
// ===================| Internal functions: middle level |====================
// =====================| Internal functions: low level |=====================
static int _uart_put_char(char c_character, FILE *ps_stream) {
  return (int)uart_tx(&c_character, sizeof(c_character));
}
