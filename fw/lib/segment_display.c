/**
 * @file
 * @author Martin Stejskal
 * @brief Driver for old school segment display
 */
// ===============================| Includes |================================
#include "segment_display.h"

#include "config.h"
#include "gpio.h"
// ================================| Defines |================================

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================
typedef union {
  struct {
    // Expected little endian - LSb first
    uint8_t low : 4;
    uint8_t high : 4;
  } s;
  uint8_t u8_raw;
} tu_seg_disp_segments;

typedef enum {
  SEG_DISP_SELECTED_NONE,
  SEG_DISP_SELECTED_LOW,
  SEG_DISP_SELECTED_HIGH,
} te_seg_disp_selected_segment;

typedef struct {
  tu_seg_disp_segments u_segments;
  te_seg_disp_selected_segment e_sel_segment;
} ts_seg_disp_runtime;
// ===========================| Global variables |============================
/**
 * @brief Keep all runtime variables under 1 common structure
 */
ts_seg_disp_runtime ms_runtime;
// ===============================| Functions |===============================

// ===============| Internal function prototypes: high level |================
/**
 * @brief Display one segment
 *
 * Function automatically select 1st or 2nd segment. Just call it.
 */
static void _display_segment(void);
// ==============| Internal function prototypes: middle level |===============
/**
 * @brief Render given value on active segment
 *
 * @param u8_value_hex Rendered value. Range: 0x0 ~ 0xF
 */
static void _render_value(const uint8_t u8_value_hex);
// ================| Internal function prototypes: low level |================
/**
 * @brief Low level functions which display 1 character
 *
 * @{
 */
static void _render_0(void);
static void _render_1(void);
static void _render_2(void);
static void _render_3(void);
static void _render_4(void);
static void _render_5(void);
static void _render_6(void);
static void _render_7(void);
static void _render_8(void);
static void _render_9(void);
static void _render_a(void);
static void _render_b(void);
static void _render_c(void);
static void _render_d(void);
static void _render_e(void);
static void _render_f(void);
static void _render_error(void);
static void _set_dot(const bool b_en_dot);
/**
 * @}
 */

// =========================| High level functions |==========================
void seg_disp_init(void) {
  // Set GPIO to output, but keep them off
  const te_gpio_cfg s_cfg = {
      .e_dir = GPIO_DIR_OUT,
      .out.b_out_value = 0,
  };

  gpio_configure_pin(IO_SEGMENT_SEL_H, s_cfg);
  gpio_configure_pin(IO_SEGMENT_SEL_L, s_cfg);
  gpio_configure_pin(IO_SEGMENT_A, s_cfg);
  gpio_configure_pin(IO_SEGMENT_B, s_cfg);
  gpio_configure_pin(IO_SEGMENT_C, s_cfg);
  gpio_configure_pin(IO_SEGMENT_D, s_cfg);
  gpio_configure_pin(IO_SEGMENT_E, s_cfg);
  gpio_configure_pin(IO_SEGMENT_F, s_cfg);
  gpio_configure_pin(IO_SEGMENT_G, s_cfg);
  gpio_configure_pin(IO_SEGMENT_DP, s_cfg);

  // Clean up both segments, so far all segments will be disabled
  ms_runtime.u_segments.u8_raw = 0;
  ms_runtime.e_sel_segment = SEG_DISP_SELECTED_NONE;
}

void seg_disp_refresh(void) {
  if (ms_runtime.e_sel_segment == SEG_DISP_SELECTED_NONE) {
    // Nothing to do
  } else {
    // Some segment is selected -> display something
    _display_segment();
  }
}
// ========================| Middle level functions |=========================
void seg_disp_set_value_dec(const uint8_t u8_value_dec) {
  if (u8_value_dec >= 100) {
    // Display it as 99 with enabled dots
    ms_runtime.u_segments.u8_raw = 0x99;
    _set_dot(1);

  } else {
    ms_runtime.u_segments.s.high = u8_value_dec / 10;
    ms_runtime.u_segments.s.low = u8_value_dec % 10;
    _set_dot(0);
  }

  _display_segment();
}

void seg_disp_set_value_hex(const uint8_t u8_value_hex) {
  ms_runtime.u_segments.u8_raw = u8_value_hex;

  // Clean up if previously set
  _set_dot(0);

  _display_segment();
}

void seg_disp_turn_off(void) {
  // Just disable drivers to the power transistors
  gpio_set_pin(IO_SEGMENT_SEL_H, 0);
  gpio_set_pin(IO_SEGMENT_SEL_L, 0);

  ms_runtime.e_sel_segment = SEG_DISP_SELECTED_NONE;
}
// ==========================| Low level functions |==========================

// ====================| Internal functions: high level |=====================
static void _display_segment(void) {
  if (ms_runtime.e_sel_segment == SEG_DISP_SELECTED_LOW) {
    // Expect that previously was enabled "high" segment -> disable it during
    // reconfiguration phase
    gpio_set_pin(IO_SEGMENT_SEL_H, 0);

    _render_value(ms_runtime.u_segments.s.low);

    // Make shine "low" segment
    gpio_set_pin(IO_SEGMENT_SEL_L, 1);

    // Select "high" segment for next time
    ms_runtime.e_sel_segment = SEG_DISP_SELECTED_HIGH;

  } else {
    // Selected segment is "high" one. Expect that previously was enabled "low"
    // segment -> disable it during reconfiguration phase
    gpio_set_pin(IO_SEGMENT_SEL_L, 0);

    _render_value(ms_runtime.u_segments.s.high);

    // Make shine "high" segment
    gpio_set_pin(IO_SEGMENT_SEL_H, 1);

    // Select "low" segment for next time
    ms_runtime.e_sel_segment = SEG_DISP_SELECTED_LOW;
  }
}
// ===================| Internal functions: middle level |====================
static void _render_value(const uint8_t u8_value_hex) {
  switch (u8_value_hex) {
    case 0:
      _render_0();
      break;

    case 1:
      _render_1();
      break;

    case 2:
      _render_2();
      break;

    case 3:
      _render_3();
      break;

    case 4:
      _render_4();
      break;

    case 5:
      _render_5();
      break;

    case 6:
      _render_6();
      break;

    case 7:
      _render_7();
      break;

    case 8:
      _render_8();
      break;

    case 9:
      _render_9();
      break;

    case 0xA:
      _render_a();
      break;

    case 0xB:
      _render_b();
      break;

    case 0xC:
      _render_c();
      break;

    case 0xD:
      _render_d();
      break;

    case 0xE:
      _render_e();
      break;

    case 0xF:
      _render_f();
      break;

    default:
      // Need to somehow display error. Power on only DP. This is internal error
      _render_error();
  }
}
// =====================| Internal functions: low level |=====================
static void _render_0(void) {
  // Inverted logic. Enable segment by low level
  gpio_set_pin(IO_SEGMENT_A, 0);
  gpio_set_pin(IO_SEGMENT_B, 0);
  gpio_set_pin(IO_SEGMENT_C, 0);
  gpio_set_pin(IO_SEGMENT_D, 0);
  gpio_set_pin(IO_SEGMENT_E, 0);
  gpio_set_pin(IO_SEGMENT_F, 0);
  gpio_set_pin(IO_SEGMENT_G, 1);
}
static void _render_1(void) {
  gpio_set_pin(IO_SEGMENT_A, 1);
  gpio_set_pin(IO_SEGMENT_B, 0);
  gpio_set_pin(IO_SEGMENT_C, 0);
  gpio_set_pin(IO_SEGMENT_D, 1);
  gpio_set_pin(IO_SEGMENT_E, 1);
  gpio_set_pin(IO_SEGMENT_F, 1);
  gpio_set_pin(IO_SEGMENT_G, 1);
}
static void _render_2(void) {
  gpio_set_pin(IO_SEGMENT_A, 0);
  gpio_set_pin(IO_SEGMENT_B, 0);
  gpio_set_pin(IO_SEGMENT_C, 1);
  gpio_set_pin(IO_SEGMENT_D, 0);
  gpio_set_pin(IO_SEGMENT_E, 0);
  gpio_set_pin(IO_SEGMENT_F, 1);
  gpio_set_pin(IO_SEGMENT_G, 0);
}
static void _render_3(void) {
  gpio_set_pin(IO_SEGMENT_A, 0);
  gpio_set_pin(IO_SEGMENT_B, 0);
  gpio_set_pin(IO_SEGMENT_C, 0);
  gpio_set_pin(IO_SEGMENT_D, 0);
  gpio_set_pin(IO_SEGMENT_E, 1);
  gpio_set_pin(IO_SEGMENT_F, 1);
  gpio_set_pin(IO_SEGMENT_G, 0);
}
static void _render_4(void) {
  gpio_set_pin(IO_SEGMENT_A, 1);
  gpio_set_pin(IO_SEGMENT_B, 0);
  gpio_set_pin(IO_SEGMENT_C, 0);
  gpio_set_pin(IO_SEGMENT_D, 1);
  gpio_set_pin(IO_SEGMENT_E, 1);
  gpio_set_pin(IO_SEGMENT_F, 0);
  gpio_set_pin(IO_SEGMENT_G, 0);
}
static void _render_5(void) {
  gpio_set_pin(IO_SEGMENT_A, 0);
  gpio_set_pin(IO_SEGMENT_B, 1);
  gpio_set_pin(IO_SEGMENT_C, 0);
  gpio_set_pin(IO_SEGMENT_D, 0);
  gpio_set_pin(IO_SEGMENT_E, 1);
  gpio_set_pin(IO_SEGMENT_F, 0);
  gpio_set_pin(IO_SEGMENT_G, 0);
}
static void _render_6(void) {
  gpio_set_pin(IO_SEGMENT_A, 0);
  gpio_set_pin(IO_SEGMENT_B, 1);
  gpio_set_pin(IO_SEGMENT_C, 0);
  gpio_set_pin(IO_SEGMENT_D, 0);
  gpio_set_pin(IO_SEGMENT_E, 0);
  gpio_set_pin(IO_SEGMENT_F, 0);
  gpio_set_pin(IO_SEGMENT_G, 0);
}
static void _render_7(void) {
  gpio_set_pin(IO_SEGMENT_A, 0);
  gpio_set_pin(IO_SEGMENT_B, 0);
  gpio_set_pin(IO_SEGMENT_C, 0);
  gpio_set_pin(IO_SEGMENT_D, 1);
  gpio_set_pin(IO_SEGMENT_E, 1);
  gpio_set_pin(IO_SEGMENT_F, 1);
  gpio_set_pin(IO_SEGMENT_G, 1);
}
static void _render_8(void) {
  gpio_set_pin(IO_SEGMENT_A, 0);
  gpio_set_pin(IO_SEGMENT_B, 0);
  gpio_set_pin(IO_SEGMENT_C, 0);
  gpio_set_pin(IO_SEGMENT_D, 0);
  gpio_set_pin(IO_SEGMENT_E, 0);
  gpio_set_pin(IO_SEGMENT_F, 0);
  gpio_set_pin(IO_SEGMENT_G, 0);
}
static void _render_9(void) {
  gpio_set_pin(IO_SEGMENT_A, 0);
  gpio_set_pin(IO_SEGMENT_B, 0);
  gpio_set_pin(IO_SEGMENT_C, 0);
  gpio_set_pin(IO_SEGMENT_D, 0);
  gpio_set_pin(IO_SEGMENT_E, 1);
  gpio_set_pin(IO_SEGMENT_F, 0);
  gpio_set_pin(IO_SEGMENT_G, 0);
}
static void _render_a(void) {
  gpio_set_pin(IO_SEGMENT_A, 0);
  gpio_set_pin(IO_SEGMENT_B, 0);
  gpio_set_pin(IO_SEGMENT_C, 0);
  gpio_set_pin(IO_SEGMENT_D, 1);
  gpio_set_pin(IO_SEGMENT_E, 0);
  gpio_set_pin(IO_SEGMENT_F, 0);
  gpio_set_pin(IO_SEGMENT_G, 0);
}
static void _render_b(void) {
  gpio_set_pin(IO_SEGMENT_A, 1);
  gpio_set_pin(IO_SEGMENT_B, 1);
  gpio_set_pin(IO_SEGMENT_C, 0);
  gpio_set_pin(IO_SEGMENT_D, 0);
  gpio_set_pin(IO_SEGMENT_E, 0);
  gpio_set_pin(IO_SEGMENT_F, 0);
  gpio_set_pin(IO_SEGMENT_G, 0);
}
static void _render_c(void) {
  gpio_set_pin(IO_SEGMENT_A, 0);
  gpio_set_pin(IO_SEGMENT_B, 1);
  gpio_set_pin(IO_SEGMENT_C, 1);
  gpio_set_pin(IO_SEGMENT_D, 0);
  gpio_set_pin(IO_SEGMENT_E, 0);
  gpio_set_pin(IO_SEGMENT_F, 0);
  gpio_set_pin(IO_SEGMENT_G, 1);
}
static void _render_d(void) {
  gpio_set_pin(IO_SEGMENT_A, 1);
  gpio_set_pin(IO_SEGMENT_B, 0);
  gpio_set_pin(IO_SEGMENT_C, 0);
  gpio_set_pin(IO_SEGMENT_D, 0);
  gpio_set_pin(IO_SEGMENT_E, 0);
  gpio_set_pin(IO_SEGMENT_F, 1);
  gpio_set_pin(IO_SEGMENT_G, 0);
}
static void _render_e(void) {
  gpio_set_pin(IO_SEGMENT_A, 0);
  gpio_set_pin(IO_SEGMENT_B, 1);
  gpio_set_pin(IO_SEGMENT_C, 1);
  gpio_set_pin(IO_SEGMENT_D, 0);
  gpio_set_pin(IO_SEGMENT_E, 0);
  gpio_set_pin(IO_SEGMENT_F, 0);
  gpio_set_pin(IO_SEGMENT_G, 0);
}
static void _render_f(void) {
  gpio_set_pin(IO_SEGMENT_A, 0);
  gpio_set_pin(IO_SEGMENT_B, 1);
  gpio_set_pin(IO_SEGMENT_C, 1);
  gpio_set_pin(IO_SEGMENT_D, 1);
  gpio_set_pin(IO_SEGMENT_E, 0);
  gpio_set_pin(IO_SEGMENT_F, 0);
  gpio_set_pin(IO_SEGMENT_G, 0);
}
static void _render_error(void) {
  gpio_set_pin(IO_SEGMENT_A, 1);
  gpio_set_pin(IO_SEGMENT_B, 1);
  gpio_set_pin(IO_SEGMENT_C, 1);
  gpio_set_pin(IO_SEGMENT_D, 1);
  gpio_set_pin(IO_SEGMENT_E, 1);
  gpio_set_pin(IO_SEGMENT_F, 1);
  gpio_set_pin(IO_SEGMENT_G, 1);
  gpio_set_pin(IO_SEGMENT_DP, 0);
}

static void _set_dot(const bool b_en_dot) {
  if (b_en_dot) {
    gpio_set_pin(IO_SEGMENT_DP, 0);
  } else {
    gpio_set_pin(IO_SEGMENT_DP, 1);
  }
}
