/**
 * @file
 * @author Martin Stejskal
 * @brief Print implementation for AVR
 *
 * It uses UART interface as print output
 */
#ifndef __PRINT_H__
#define __PRINT_H__
// ===============================| Includes |================================

// ================================| Defines |================================

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================

// ===========================| Global variables |============================

// ===============================| Functions |===============================
// =========================| High level functions |==========================
/**
 * @brief Setup UART interface so "printf()" will be redirected there
 */
void print_init(void);
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================

#endif  // __PRINT_H__
