/**
 * @file
 * @author Martin Stejskal
 * @brief Read value from inputs
 */
// ===============================| Includes |================================
#include "inputs.h"

#include "config.h"
#include "gpio.h"
// ================================| Defines |================================

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================

// ===========================| Global variables |============================

// ===============================| Functions |===============================

// ===============| Internal function prototypes: high level |================
// ==============| Internal function prototypes: middle level |===============
// ================| Internal function prototypes: low level |================

// =========================| High level functions |==========================
void inputs_init(void) {
  // Initialize GPIO as input
  te_gpio_cfg s_cfg = {
      .e_dir = GPIO_DIR_IN,
      // Internal pull ups are not sufficient -> used external anyways
      .in.e_pull_up_down = GPIO_NO_PULL_UP_PULL_DOWN,
  };

  gpio_configure_pin(IO_INPUT_BTN_UP, s_cfg);
  gpio_configure_pin(IO_INPUT_BTN_DOWN, s_cfg);
  gpio_configure_pin(IO_INPUT_BTN_NEXT, s_cfg);
  gpio_configure_pin(IO_INPUT_TRIG_0, s_cfg);
  gpio_configure_pin(IO_INPUT_TRIG_1, s_cfg);
  gpio_configure_pin(IO_INPUT_TRIG_2, s_cfg);
  gpio_configure_pin(IO_INPUT_TRIG_3, s_cfg);
}

bool inputs_get(te_input_sources e_source) {
  // Due to pull up, logic is inverted -> false will be returned if invalid
  // input source
  bool b_in_value = true;

  switch (e_source) {
    case INPUT_BTN_UP:
      gpio_get_pin(IO_INPUT_BTN_UP, &b_in_value);
      break;

    case INPUT_BTN_DOWN:
      gpio_get_pin(IO_INPUT_BTN_DOWN, &b_in_value);
      break;

    case INPUT_BTN_NEXT:
      gpio_get_pin(IO_INPUT_BTN_NEXT, &b_in_value);
      break;

    case INPUT_TRIG_0:
      gpio_get_pin(IO_INPUT_TRIG_0, &b_in_value);
      break;

    case INPUT_TRIG_1:
      gpio_get_pin(IO_INPUT_TRIG_1, &b_in_value);
      break;

    case INPUT_TRIG_2:
      gpio_get_pin(IO_INPUT_TRIG_2, &b_in_value);
      break;

    case INPUT_TRIG_3:
      gpio_get_pin(IO_INPUT_TRIG_3, &b_in_value);
      break;

    default:
      // Do not do anything. Keep default value
      break;
  }

  // Logic is inverted due to pull up resistors
  return !b_in_value;
}
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================

// ====================| Internal functions: high level |=====================
// ===================| Internal functions: middle level |====================
// =====================| Internal functions: low level |=====================
