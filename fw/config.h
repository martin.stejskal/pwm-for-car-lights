/**
 * @file
 * @author Martin Stejskal
 * @brief Configuration file
 *
 * Mainly hardware related stuff
 */
#ifndef __CONFIG_H__
#define __CONFIG_H__
// ===============================| Includes |================================
#include "gpio.h"
// ================================| Defines |================================
/**
 * @brief Hardware related stuff
 *
 * @{
 */
#define UART_BAUDRATE (9600UL)

#define IO_LED_0 (PB_6)
#define IO_LED_1 (PB_5)
#define IO_LED_2 (PB_7)
#define IO_LED_3 (PB_4)

#define IO_PWM_0 (PB_3)
#define IO_PWM_1 (PD_5)
#define IO_PWM_2 (PD_4)
#define IO_PWM_3 (PD_7)

#define IO_SEGMENT_SEL_H (PC_7)
#define IO_SEGMENT_SEL_L (PC_6)
#define IO_SEGMENT_A (PA_1)
#define IO_SEGMENT_B (PA_3)
#define IO_SEGMENT_C (PA_5)
#define IO_SEGMENT_D (PA_6)
#define IO_SEGMENT_E (PA_4)
#define IO_SEGMENT_F (PA_2)
#define IO_SEGMENT_G (PA_0)
#define IO_SEGMENT_DP (PA_7)

#define IO_INPUT_BTN_UP (PB_0)
#define IO_INPUT_BTN_DOWN (PB_1)
#define IO_INPUT_BTN_NEXT (PB_2)
#define IO_INPUT_TRIG_0 (PC_0)
#define IO_INPUT_TRIG_1 (PC_1)
#define IO_INPUT_TRIG_2 (PC_2)
#define IO_INPUT_TRIG_3 (PC_3)

/**
 * @}
 */

/**
 * @brief Debug related things
 *
 * @{
 */
#define DEBUG_ALIVE_MESSAGE (1)
#define DEBUG_INPUT_TRIGGER_EVENTS (1)
#define DEBUG_BUTTONS (0)
#define DEBUG_NO_ACTIVITY (0)
/**
 * @}
 */
// ==========================| Preprocessor checks |==========================

#endif  // __CONFIG_H__
