/**
 * @file
 * @author Martin Stejskal
 * @brief Main file for PWM 4 Car light project
 */
// ===============================| Includes |================================
#include <stdio.h>
#include <util/delay.h>

#include "config.h"
#include "gpio.h"
#include "inputs.h"
#include "led.h"
#include "nvs.h"
#include "print.h"
#include "pwm.h"
#include "segment_display.h"
// ================================| Defines |================================
/**
 * @brief Timeouts in cycles
 *
 * Since no timer is available, due to PWM functionality, time is count via
 * cycles. Not accurate, but it also works. Values below are empirically
 * estimated. Also note that cycle timing is variable.
 *
 * @{
 */
/**
 * @brief Inactivity counter
 *
 */
#define NO_ACTIVITY_TIMEOUT (40000)

/**
 * @brief When button pressed, ignore press for following number of cycles
 *
 */
#define BTN_IGNORE_TIMEOUT (1000)

/**
 * @}
 */
// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================
#if NO_ACTIVITY_TIMEOUT < BTN_IGNORE_TIMEOUT
#error "No activity timeout have to be bigger than button ignore timeout"
#endif
// =======================| Structures, enumerations |========================

// ===========================| Global variables |============================

// ===============================| Functions |===============================

// ===============| Internal function prototypes: high level |================
/**
 * @brief Function that cares about input trigger events
 */
static void _read_trigger_task(void);

/**
 * @brief Function that handle button events
 */
static void _read_buttons_task(void);
// ==============| Internal function prototypes: middle level |===============
// ================| Internal function prototypes: low level |================
/**
 * @brief Increase PWM value and update segment display
 *
 * @param e_pwm_ch PWM channel
 */
static void _pwm_increase_update_seg_disp(const te_pwm_channel e_pwm_ch);

/**
 * @brief Decrease PWM value and update segment display
 *
 * @param e_pwm_ch PWM channel
 */
static void _pwm_decrease_update_seg_disp(const te_pwm_channel e_pwm_ch);

/**
 * @brief Convert PWM channel to LED index
 *
 * Function helps to align PWM channel with LED indication of active cahnnel
 *
 * @param e_pwm_ch PWM channel
 * @return LED index
 */
static te_led_select _pwm_ch_to_led(const te_pwm_channel e_pwm_ch);
// =========================| High level functions |==========================
int main(void) {
  // Need to initialize print functions first, so "printf()" can be used
  print_init();

  // First, initialize PWM and restore values. Then continue with user
  // interface.
  pwm_init();
  if (DEBUG_ALIVE_MESSAGE) {
    printf("\n\n\nPWM 4 Car light alive!\n");
  }

  _read_trigger_task();

  led_init();
  seg_disp_init();
  inputs_init();

  while (1) {
    // One "round" of this loop takes at about 10 ms (when printing to UART)
    _read_trigger_task();
    _read_buttons_task();
  }

  return 0;
}
// ========================| Middle level functions |=========================

// ==========================| Low level functions |==========================

// ====================| Internal functions: high level |=====================
static void _read_trigger_task(void) {
  // Tracks inputs and when changed, it will perform action
  static bool b_inputs[PWM_CH_ALL];

  // Current input value inside loop
  bool b_input;
  // Assigned PWM channel to given input
  te_pwm_channel e_pwm_ch;

  for (te_input_sources e_input = (te_input_sources)INPUT_TRIG_0;
       e_input <= INPUT_TRIG_3; e_input++) {
    b_input = inputs_get(e_input);

    // Input trigger value have offset -> need to align with "zero"
    e_pwm_ch = e_input - INPUT_TRIG_0;

    if (b_input != b_inputs[e_pwm_ch]) {
      b_inputs[e_pwm_ch] = b_input;

      if (b_input) {
        // Enable PWM channel
        pwm_set(e_pwm_ch, nvs_load_pwm(e_pwm_ch));

        if (DEBUG_INPUT_TRIGGER_EVENTS) {
          printf("Trigger enable channel %d\n", e_pwm_ch);
        }

      } else {
        // Disable PWM channel
        pwm_set(e_pwm_ch, 0);

        if (DEBUG_INPUT_TRIGGER_EVENTS) {
          printf("Trigger disable channel %d\n", e_pwm_ch);
        }
      }
    }
  }
}

static void _read_buttons_task(void) {
  // Since we're out of HW timers, this variable keeps information about time
  // as number of task executions. Not accurate, but it somehow works too
  static uint32_t u32_time = 0;
  static uint32_t u32_last_activity_timestamp = 0;
  static uint32_t u32_ignore_btns_timeout = 0;

  // Signalize which PWM channel is selected
  static te_pwm_channel e_selected_pwm_ch = 0;

  // Signalize if segment display and LED are active
  static bool b_led_active = false;

  static uint8_t u8_pwm_backup;

  // Always increase timer - no exception
  u32_time++;

  // Load button states into variable, so they can be processed later if needed
  bool b_btns[INPUT_BTN_NEXT + 1];
  b_btns[INPUT_BTN_UP] = inputs_get(INPUT_BTN_UP);
  b_btns[INPUT_BTN_DOWN] = inputs_get(INPUT_BTN_DOWN);
  b_btns[INPUT_BTN_NEXT] = inputs_get(INPUT_BTN_NEXT);

  if (b_btns[INPUT_BTN_UP] || b_btns[INPUT_BTN_DOWN] ||
      b_btns[INPUT_BTN_NEXT]) {
    u32_last_activity_timestamp = u32_time;
    // Low chance, but we might hit button when timer value was 0 -> the 0 value
    // for last activity timestamp have special meaning (initialization phase).
    // So for that case value is increased by 1
    if (u32_last_activity_timestamp == 0) {
      u32_last_activity_timestamp++;
    }
  }

  uint32_t u32_time_since_last_activity =
      u32_time - u32_last_activity_timestamp;

  // If no recent activity or no button was pressed yet (last activity timestamp
  // still 0), exit now.
  if ((u32_time_since_last_activity > NO_ACTIVITY_TIMEOUT) ||
      (u32_last_activity_timestamp == 0)) {
    // Turn off segment display or LED if not done yet
    if (b_led_active) {
      seg_disp_turn_off();
      led_set(LED_ALL, 0);

      // Restore previous value
      pwm_set(e_selected_pwm_ch, u8_pwm_backup);

      b_led_active = false;
      u32_ignore_btns_timeout = 0;

      if (DEBUG_NO_ACTIVITY) {
        printf("No activity detected. Disabling segment display and LEDs\n");
      }
    }

    return;
  }

  // LED and segment display not active yet -> set them
  if (!b_led_active) {
    led_set(LED_ALL, 0);
    led_set(_pwm_ch_to_led(e_selected_pwm_ch), 1);

    uint8_t u8_pwm_percentage = nvs_load_pwm(e_selected_pwm_ch);
    // Show user current value regardless of current trigger. But make backup
    // of current PWM settings, so after saving settings it will return to
    // previous behavior
    u8_pwm_backup = pwm_get(e_selected_pwm_ch);
    pwm_set(e_selected_pwm_ch, u8_pwm_percentage);

    seg_disp_set_value_dec(u8_pwm_percentage);

    b_led_active = true;
  }

  // Need to refresh segment display always
  seg_disp_refresh();

  // If buttons should be ignored. Do not even try to process them
  if (u32_ignore_btns_timeout) {
    u32_ignore_btns_timeout--;
    return;
  }

  // Process buttons
  if (b_btns[INPUT_BTN_UP]) {
    _pwm_increase_update_seg_disp(e_selected_pwm_ch);

    if (DEBUG_BUTTONS) {
      printf("Pressed button UP\n");
    }
  }

  if (b_btns[INPUT_BTN_DOWN]) {
    _pwm_decrease_update_seg_disp(e_selected_pwm_ch);
    if (DEBUG_BUTTONS) {
      printf("Pressed button DOWN\n");
    }
  }

  if (b_btns[INPUT_BTN_NEXT]) {
    // Save current PWM value to NVS
    nvs_save_pwm(e_selected_pwm_ch, pwm_get(e_selected_pwm_ch));

    // If PWM backup was zero, it means that light is off -> should be restored.
    // But if PWM backup value was different, drop it, since new value was set
    if (u8_pwm_backup == 0) {
      pwm_set(e_selected_pwm_ch, u8_pwm_backup);
    }

    e_selected_pwm_ch++;
    if (e_selected_pwm_ch >= PWM_CH_ALL) {
      e_selected_pwm_ch = PWM_CH_0;
    }

    // Tell that "LED is not active", so all values will be reloaded (segment
    // display, LED);
    b_led_active = false;

    if (DEBUG_BUTTONS) {
      printf("Pressed button NEXT\n");
    }
  }

  if (b_btns[INPUT_BTN_UP] || b_btns[INPUT_BTN_DOWN] ||
      b_btns[INPUT_BTN_NEXT]) {
    // Tell how long should be button actions ignored
    u32_ignore_btns_timeout = BTN_IGNORE_TIMEOUT;
  }
}
// ===================| Internal functions: middle level |====================
// =====================| Internal functions: low level |=====================
static void _pwm_increase_update_seg_disp(const te_pwm_channel e_pwm_ch) {
  uint8_t u8_pwm_percentage = pwm_get(e_pwm_ch);

  if (u8_pwm_percentage >= 100) {
    // It does not make sense to increase value, since this is maximum
    return;
  }

  u8_pwm_percentage++;

  pwm_set(e_pwm_ch, u8_pwm_percentage);

  seg_disp_set_value_dec(u8_pwm_percentage);
}

static void _pwm_decrease_update_seg_disp(const te_pwm_channel e_pwm_ch) {
  uint8_t u8_pwm_percentage = pwm_get(e_pwm_ch);

  if (u8_pwm_percentage == 0) {
    // Can not go lower actually. Nothing to do.
    return;
  }

  u8_pwm_percentage--;

  pwm_set(e_pwm_ch, u8_pwm_percentage);

  seg_disp_set_value_dec(u8_pwm_percentage);
}

static te_led_select _pwm_ch_to_led(const te_pwm_channel e_pwm_ch) {
  switch (e_pwm_ch) {
    case PWM_CH_0:
      return LED_0;
    case PWM_CH_1:
      return LED_1;
    case PWM_CH_2:
      return LED_2;
    case PWM_CH_3:
      return LED_3;
    default:
      // Invalid value. This should not happen
      return -1;
  }
}
