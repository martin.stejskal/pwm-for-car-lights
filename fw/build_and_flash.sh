#!/bin/bash
# RUN COMMAND FROM FW DIRECTORY

# Exit when any command fails
set -e

./build.sh

# Flash
cmake --build build/ -t upload_PwmForCarLights # --verbose
