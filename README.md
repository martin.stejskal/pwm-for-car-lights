# About
 * Did you changed regular bulbs by LED, but they shine bit too much? This is
   the solution! Reduce output power through energy efficient PWM driver!
 * It allow to set 4 independent high speed PWM channels driven by HW
   peripheral.
 * Output power percentage is displayed via segment display when setting them.

![img](doc/PWM_4_car_light-01.jpg)

# Setting
 * By default, all PWM channels are set to 100% duty cycle.
 * By pressing button *UP* you will increase output power. You can see current
   PWM settings as percentage on segment display.
 * By pressing button *DOWN* you will decrease output power. Segment display
   will show you current PWM settings.
 * Once you're happy with set value, press *NEXT* button. That will save your
   settings and move to another channel.
 * When setting PWM channel, power output is **always** active, so you can
   check output power.

# How it works
 * The input trigger `I1` `I2` `I3` `I4` (`J2` bottom connector on left side)
   power on MCU. Recommended power supply voltage is from 5 ~ 15 V. Current
   consumption is about 10 mA.
 * The power supply have to be permanently connected to `J1` connector.
 * Power output is at signals `O1` `O2` `O3` `O4` . See connector `J5` at
   bottom middle.
 * MCU keep scanning all input trigger channels. If any new input trigger
   channel is enabled, it immediately enable PWM channel for that input trigger
   channel. Once input trigger channel is disabled, PWM output is also
   disabled.
 * MCU active until at least 1 input trigger channel is active.

# Download HEX
 * HEX files are automatically build :) You can download latest below
 * [CI job artifact](https://gitlab.com/martin.stejskal/pwm-for-car-lights/-/jobs/artifacts/main/download?job=build_main)

# HW
 * It is **highly recommended** to use version
   [01](hw/export/PWM_4_car_light-01_pcb.pdf). Previous version had problems
   with programming pins and floating input trigger inputs, which caused
   unwanted behavior when detecting trigger event source.
 * For more details, see [schematic](hw/export/PWM_4_car_light-01_sch.pdf)

# Mechanical
 * In the [mech](mech) folder you will find all source & export files.
 * Recommend to use PETG to print case. The PLA might "melt" inside car during
   hot summer days. Better to use some white material in order to better see
   segments hidden under 1st layer. In worst case you can cut out the window
   for segment display.

![case](doc/PWM_4_car_case.jpg)
